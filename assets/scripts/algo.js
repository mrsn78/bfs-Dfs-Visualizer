class Node {
    constructor(id) {
        this.id = id;
        this.visited = false;
        this.adjacentNodes = [];
    }

    addEdge(node) {
        this.adjacentNodes.push(node);
    }
}

class Step {
    constructor(type, nodeId) {
        this.type = type;
        this.nodeId = nodeId;
    }
}

const stepType = Object.freeze({"visit": 1, "moveTo": 2, "check": 3});
const algorithm = Object.freeze({"BFS": 1, "DFS": 2});

let queue = [];
let nodes = [];

let stepList = [];
let visitedList = [];
let currentStep = 0;
let currentNode = 0;

let startNode = 0;
let endNode = -1;

function startAlgo(algo) {
    //reset just to be sure
    queue = [];
    stepList = [];
    visitedList = [];
    currentStep = 0;
    currentNode = startNode;

    for (let node of nodes) {
        node.adjacentNodes.sort(compareIds);
    }
    if (algo == algorithm.BFS) {
        nodes[startNode].visited = true;
        stepList.push(new Step(stepType.visit, startNode));
        queue.push(startNode);
        startBFS(startNode, endNode);
    } else if (algo == algorithm.DFS) {
        startDFS(startNode, endNode);
    }
}

function startBFS(startNode, endNode) {
    while (queue.length != 0) {
        let node = nodes[queue.shift()];
        stepList.push(new Step(stepType.moveTo, node.id));
        if (node.id == endNode) {
            return true;
        }
        for (let neighbour of node.adjacentNodes) {
            stepList.push(new Step(stepType.check, neighbour.id))
            if (!neighbour.visited) {
                queue.push(neighbour.id);
                neighbour.visited = true;
                stepList.push(new Step(stepType.visit, neighbour.id));
            }
        }
    }
    return false;
}

function startDFS(startNode, endNode) {
    return recDFS(startNode, endNode);
}

function recDFS(nodeId, endNode) {
    nodes[nodeId].visited = true;
    stepList.push(new Step(stepType.visit, nodeId));
    stepList.push(new Step(stepType.moveTo, nodeId));
    if (nodeId == endNode) {
        return true;
    } else {
        for (let node of nodes[nodeId].adjacentNodes) {
            stepList.push(new Step(stepType.check, node.id));
            if (!node.visited) {
                if (recDFS(node.id, endNode)) {
                    return true;
                }
            }
        }
        return false;
    }
}

function compareIds(a, b) {
    return a.id - b.id;
}