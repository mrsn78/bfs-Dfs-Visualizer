let speedMultiplier = 3;
let algoPaused = false;
let algo = algorithm.DFS;
let blockReset = false
let directed = true;
let displayNodes = new Array(10); //Length 4-16
let nodeTxt = new Array(displayNodes.length);
let displayEdges = [];
let xOffset = "3";
let yOffset = "3";

let speedSlider = document.getElementById("speedSlider");
let startButton = document.getElementById("startButton");
let navigation = document.getElementById("navigation");
let svg = document.getElementById("graphSvg");
let dfsButton = document.getElementById("dfsButton");
let bfsButton = document.getElementById("bfsButton");
let dfsText = document.getElementById("dfsText");
let bfsText = document.getElementById("bfsText");
let slider = document.getElementById("directedSlider");

dfsButton.disabled = true;

let sliderAnim = anime.timeline({targets: "#directedSliderHandle", autoplay: false}).add({
    translateX: {value: "+= 0.9rem", easing: "easeInQuart"},
    scale: 0.5,
    background: "#ADADAD", //Knopf beim Übergang
    easing: "easeInCubic",
    duration: 200
}).add({
    translateX: {value: "+= 0.9rem", easing: "easeOutQuart"},
    scale: 1,
    easing: "easeInCubic",
    background: "#F8F8FFFF", //Knopf wenn rechts
    duration: 200
}).add({
    targets: slider,
    easing: "easeInCubic",
    background: "#7b0828", //Hintergrund bei Knopf rechts
    duration: 400
}, 0);

//Listeners
document.getElementById("help").style.cursor = "help";

document.getElementById("help").onmouseover = function () {
    document.getElementById("popup").classList.add("show");
};
document.getElementById("help").onmouseout = function () {
    document.getElementById("popup").classList.remove("show");
};

document.getElementById("mobileInfoButton").addEventListener("click", function () {
    document.getElementById("overlay").classList.add("active");

    let id = "Text"
    if(algo === algorithm.DFS){
        id = "dfs" + id
    } else {
        id = "bfs" + id
    }

    let text = document.getElementById(id).innerText
    document.getElementById("overlayWindow").firstElementChild.innerHTML = text

    anime ({
        targets: "#overlayWindow",
        duration: 500,
        opacity: [0, 1],
        translateY: ["+=100", 0],
        easing: "easeOutBack"
    })
})

document.getElementById("overlayBG").addEventListener("click", function (event) {
    document.getElementById("overlay").classList.remove("active");
})

speedSlider.addEventListener("change", (event) => {
    if (event.target.id === "speedSlider") {
        speedMultiplier = reverseRange(event.target.value, 1, 5);
    }
});

speedSlider.addEventListener("mousedown", (event) => {
    if (event.target.id === "speedSlider" && document.getElementById("playIcon").hidden && !algoPaused) {
        algoPaused = true
    }
});

speedSlider.addEventListener("mouseup", () => {
    if (startButton.classList.contains("hidden") && document.getElementById("playIcon").hidden && algoPaused) {
        speedMultiplier = reverseRange(document.getElementById("speedSlider").value, 1, 5);
        algoPaused = false
        playAlgorithm()
    }
});

startButton.addEventListener("click", startButtonClicked);

dfsButton.addEventListener("click", algoButtonClicked);
bfsButton.addEventListener("click", algoButtonClicked);

for (let navigationElement of navigation.children) {
    navigationElement.addEventListener("click", navigationClicked);
}

document.getElementsByTagName("body")[0].onload = function () {
    bodyLoaded()
};

document.getElementById("newGraphButton").addEventListener("click", resetGraph);

document.getElementById("startPoint").addEventListener("input", (event) => {
    if (event.target.id === "startPoint") {
        startNode = event.target.value;
    }
});

document.getElementById("endPoint").addEventListener("input", (event) => {
    if (event.target.id === "endPoint") {
        endNode = event.target.value;
    }
});

slider.addEventListener("click", (event) => {
    if (!sliderAnim.began || sliderAnim.completed) {
        if (event.target.classList.contains("undirected")) {
            directed = true;
            sliderAnim.reverse();
            sliderAnim.play();
            event.target.classList.remove("undirected");
        } else {
            directed = false;
            event.target.classList.add("undirected");
            if (sliderAnim.reversed) {
                sliderAnim.reverse();
            }
            sliderAnim.play();
        }
        resetGraph()
    }
});

generateNodes();

//Functions
function startButtonClicked(event) {
    let startPoint = document.getElementById("startPoint");
    let endPoint = document.getElementById("endPoint");

    if (startPoint.validity.rangeOverflow || startPoint.validity.rangeUnderflow) {
        startPoint.reportValidity();
        return;
    }
    if (endPoint.validity.rangeOverflow || endPoint.validity.rangeUnderflow) {
        endPoint.reportValidity();
        return;
    }

    if (event.target.id === "startButton") {
        animateNavigation(true);
        document.getElementById("playIcon").hidden = true;
        document.getElementById("pauseIcon").hidden = false;
        algoPaused = false;
        startAlgo(algo);
        playAlgorithm(0);
    }
}

function animateNavigation(animateIn) {
    if (animateIn) {
        let pos = startButton.getBoundingClientRect().x + startButton.getBoundingClientRect().width / 2;
        navigation.classList.remove("hidden");
        startButton.classList.add("hidden");
        document.getElementById("startPoint").disabled = true;
        document.getElementById("endPoint").disabled = true;
        let iconWidth = navigation.firstElementChild.getBoundingClientRect().width;

        anime({
            targets: ".iconContainer object",
            left: pos - iconWidth / 2,
            opacity: 0,
            direction: "reverse",
            easing: "easeInQuad",
            duration: 300,
            complete: function (animation) {
                let objects = animation.animatables;
                for (let object of objects) {
                    object.target.style.left = ""
                }
            }
        })
    } else {
        let pos = navigation.getBoundingClientRect().x + navigation.getBoundingClientRect().width / 2;
        let iconWidth = navigation.firstElementChild.getBoundingClientRect().width;
        document.getElementById("startPoint").disabled = false;
        document.getElementById("endPoint").disabled = false;

        anime({
            targets: ".iconContainer object",
            left: pos - iconWidth / 2,
            opacity: 0,
            loopComplete: function () {
                navigation.classList.add("hidden");
                startButton.classList.remove("hidden");
            },
            complete: function (animation) {
                let objects = animation.animatables;
                for (let object of objects) {
                    object.target.style.left = ""
                }
            },
            direction: "alternate",
            easing: "easeOutQuad",
            duration: 300
        });
    }
}

function navigationClicked(event) {
    switch (event.target.id) {
        case "skipStartIcon":
            algoPaused = false;
            playAlgorithm(currentStep, true, 0);
            break
        case "skipPrevIcon":
            algoPaused = true;
            playAlgorithm(currentStep, true);
            break
        case "playPauseContainer":
            if (currentStep !== stepList.length) {
                if (algoPaused) {
                    playAlgorithm()
                }
                algoPaused = !algoPaused;
            }
            break
        case "skipNextIcon":
            algoPaused = true;
            playAlgorithm();
            break
        case "skipEndIcon":
            algoPaused = false;
            playAlgorithm(currentStep, false, 0);
            break
    }

    document.getElementById("playIcon").hidden = !algoPaused;
    document.getElementById("pauseIcon").hidden = algoPaused;
}

function algoButtonClicked(event) {
    if (event.target.id === "dfsButton") {
        algo = algorithm.DFS;
        dfsButton.disabled = true;
        bfsButton.disabled = false;
        dfsText.style.display = "block";
        bfsText.style.display = "none";
    } else if (event.target.id === "bfsButton") {
        algo = algorithm.BFS;
        bfsButton.disabled = true;
        dfsButton.disabled = false;
        dfsText.style.display = "none";
        bfsText.style.display = "block";
    } else {
        return;
    }
    anime({
        targets: "#" + event.target.id.substring(0, 3) + "Text",
        translateX: [-500, 0],
        easing: "easeOutQuad",
        duration: 625,
        opacity: [0, 1]
    });

    if (startButton.classList.contains("hidden")) {
        resetGraph();
    }

}

function bodyLoaded() {
    anime({
        targets: dfsText,
        translateX: [-500, 0],
        easing: "easeOutQuad",
        duration: 625,
        opacity: [0, 1]
    });
    anime({
        targets: [dfsButton, bfsButton],
        scale: {
            value: [0, 1],
            easing: 'easeOutQuad',
            duration: 500
        },
        complete: function (anim) {
            for (let animatable of anim.animatables) {
                animatable.target.style.transform = ""
            }
        }
    });
}

function resetGraph() {
    if (blockReset) {
        return
    } else {
        blockReset = true
    }
    algoPaused = true;
    document.getElementById("firstStatusLine").innerHTML = ""
    document.getElementById("secondStatusLine").innerHTML = ""
    generateNodes();
    if (startButton.classList.contains("hidden")) {
        animateNavigation(false);
    }
    let queue = document.getElementById("queue");
    let sequence = document.getElementById("sequence");
    queue.innerHTML = "";
    sequence.innerHTML = "";
}



async function playAlgorithm(startStep = currentStep, reverse = false, delay = 600) {
    delay *= speedMultiplier
    currentStep = startStep;
    while (0 <= currentStep && currentStep <= stepList.length) {
        if (reverse) {
            if (currentStep !== 0) {
                prevStep(delay !== 0);
            } else {
                algoPaused = true;
                document.getElementById("playIcon").hidden = false;
                document.getElementById("pauseIcon").hidden = true;
            }
        } else {
            if (currentStep !== stepList.length) {
                nextStep(delay !== 0);
            } else {
                algoPaused = true;
                document.getElementById("playIcon").hidden = false;
                document.getElementById("pauseIcon").hidden = true;

                if (delay === 0) {
                    let idLast = document.getElementById("sequence").lastElementChild.innerHTML

                    anime({
                        targets: "#node" + idLast,
                        duration: 250,
                        strokeWidth: "0.5%",
                        easing: "easeOutQuad"
                    })
                }
            }
        }
        updateNavigationHighlighting();
        let stepPromise = stepDelay(delay);
        let result = await stepPromise;
        if (result === false) {
            return
        }
    }
}

function nextStep(animate) {
    anime({
        targets: ".edge",
        duration: 450 * speedMultiplier,
        easing: "easeInOutCubic",
        stroke: "#3e3e3e"
    });

    let step = stepList[currentStep];
    updateStatusText(currentStep)
    switch (step.type) {
        case stepType.visit:

            if (algo === algorithm.BFS) {

                queue.push(step.nodeId);


                let newP = document.createElement("p");
                let newContent = document.createTextNode(step.nodeId);
                newP.appendChild(newContent);
                let currentP = document.getElementById("queue");
                currentP.appendChild(newP);

                anime({
                    targets: newP,
                    opacity: [0, 1],
                    duration: 500 * speedMultiplier
                });

            }

            anime.timeline({
                easing: "easeOutQuad",
                duration: 250 * speedMultiplier
            }).add({
                targets: "#node" + step.nodeId,
                fill: "#7b0828"
            });
            break;
        case stepType.moveTo:

            if (algo === algorithm.BFS) {
                queue.shift();
                let currentQueue = document.getElementById("queue");


                anime({
                    complete: function () {
                        if (currentQueue.children.length !== 0) {
                            currentQueue.removeChild(currentQueue.firstElementChild);
                        }
                    },
                    targets: currentQueue.firstElementChild,
                    opacity: [1, 0],
                    duration: 500 * speedMultiplier
                });
            }

            let sequence = document.getElementById("sequence");
            let newP = document.createElement("p");
            let newContent = document.createTextNode(step.nodeId);
            newP.appendChild(newContent);
            sequence.appendChild(newP);

            let strokeAnim = anime.timeline({
                duration: 250 * speedMultiplier
            }).add({
                targets: displayNodes[currentNode],
                strokeWidth: 0,
                easing: "easeInQuad"
            })
            if (animate) {
                strokeAnim.add({
                    targets: displayNodes[step.nodeId],
                    strokeWidth: "0.5%",
                    easing: "easeOutQuad"
                });
            }
            currentNode = step.nodeId;
            break;
        case stepType.check:
            anime({
                targets: "#" + getEdgeId(nodes[currentNode], nodes[step.nodeId]),
                duration: 450 * speedMultiplier,
                easing: "easeInOutCubic",
                stroke: "#5F9EA0"
            });
            break;
    }
    currentStep++;
}

function prevStep(animate) {
    currentStep--;
    let step = stepList[currentStep];
    updateStatusText(currentStep - 1)
    switch (step.type) {
        case stepType.visit:

            if (algo === algorithm.BFS) {
                queue.pop();
                let currentQueue = document.getElementById("queue");

                anime({
                    complete: function () {
                        currentQueue.removeChild(currentQueue.lastElementChild);
                    },
                    targets: currentQueue.lastElementChild,
                    opacity: [1, 0],
                    duration: 500 * speedMultiplier
                });
            }

            anime.timeline({
                easing: "easeOutQuad",
                duration: 250 * speedMultiplier
            }).add({
                targets: "#node" + step.nodeId,
                fill: "#3e3e3e"
            });
            break;
        case stepType.moveTo:

            if (algo === algorithm.BFS) {
                queue.unshift(step.nodeId);

                let newP = document.createElement("p");
                let newContent = document.createTextNode(step.nodeId);
                newP.appendChild(newContent);
                let currentQueue = document.getElementById("queue");
                currentQueue.insertBefore(newP, currentQueue.firstElementChild);

                anime({
                    targets: newP,
                    opacity: [0, 1],
                    duration: 500 * speedMultiplier
                });
            }

            let sequence = document.getElementById("sequence");
            sequence.removeChild(sequence.lastChild);

            for (let i = currentStep - 1; i >= 0; i--) {
                if (stepList[i].type === stepType.moveTo) {
                    currentNode = stepList[i].nodeId;
                    break
                }
            }

            let timeline = anime.timeline({
                duration: 250 * speedMultiplier
            }).add({
                targets: displayNodes[step.nodeId],
                strokeWidth: 0,
                easing: "easeInQuad"
            })

            if (animate && currentNode !== step.nodeId) {
                timeline.add({
                    targets: displayNodes[currentNode],
                    strokeWidth: "0.5%",
                    easing: "easeOutQuad"
                })
            }
            break;
        case stepType.check:
            anime({
                targets: "#" + getEdgeId(nodes[currentNode], nodes[step.nodeId]),
                duration: 450 * speedMultiplier,
                easing: "easeInOutCubic",
                stroke: "#3e3e3e"
            });
            break;
    }

    if (currentStep > 0 && stepList[currentStep - 1].type === stepType.check) {
        anime({
            targets: "#" + getEdgeId(nodes[currentNode], nodes[stepList[currentStep - 1].nodeId]),
            duration: 450 * speedMultiplier,
            easing: "easeInOutCubic",
            stroke: "#5F9EA0"
        });
    }
}

async function stepDelay(delay) {
    let iteration = delay / 50;
    for (let i = 0; i < iteration; i++) {
        await sleep(50);
        if (algoPaused) {
            return false;
        }
    }
    return !algoPaused;
}

/*
Steps: Check, Visit, MoveTo
Finish Possibilities: All reachable Nodes visited (Directed, no connection), EndNode found
 */
function updateStatusText(currentStep) {
    let outputText1 = ""
    let outputText2 = ""

    let step = stepList[currentStep]
    if (currentStep === stepList.length - 1) {
        if (parseInt(step.nodeId) === parseInt(endNode.toString())) {
            outputText1 = "Der gesuchte Knoten wurde gefunden!"
        } else {
            let all = true
            for (let node of nodes) { //Test if all nodes were visited
                if (!node.visited) {
                    all = false
                    break
                }
            }
            if (all) {
                outputText1 = "Alle Knoten wurden durchlaufen!"
            } else {
                outputText1 = "Alle erreichbaren Knoten wurden durchlaufen!"
                outputText2 = "Nicht besuchte Knoten konnten aufgrund von "
                if (directed) {
                    outputText2 += "gerichteten und/oder "
                }
                outputText2 += "fehlenden Kanten nicht erreicht werden!"
            }
        }
    } else {
        if (step !== undefined) {
            switch (step.type) {
                case stepType.check:
                    outputText1 = "Teste Kante auf nicht besuchten Knoten!"
                    break;
                case stepType.visit:
                    outputText1 = "Markiere Knoten " + step.nodeId + " als \"Besucht\"!"
                    break;
                case stepType.moveTo:
                    outputText1 = "Wechsle untersuchten Knoten!"
                    break;
            }
        }
    }

    let container1 = document.getElementById("firstStatusLine");
    container1.innerHTML = ""
    let container2 = document.getElementById("secondStatusLine");
    container2.innerText = outputText2
    if (outputText2.length > 0) {
        anime({
            targets: container2,
            translateY: [-20, 0],
            opacity: {
                value: [0, 1],
                duration: 500,
                easing: "easeOutQuad"
            },
            delay: 200
        });
    }

    for (let character of outputText1) {
        let characterP = document.createElement("p")
        characterP.innerText = character
        if (character === " ") {
            characterP.innerHTML = "&nbsp;"
        }
        container1.appendChild(characterP)
    }
    anime({
        targets: container1.children,
        translateY: [-10, 0],
        opacity: {
            value: [0, 1],
            duration: 50,
            easing: "easeOutQuad"
        },
        delay: anime.stagger(10, {from: "center", easing: "easeInQuad"}),
        easing: "easeOutQuad",
        duration: 300
    });
}

function updateNavigationHighlighting() {
    if (currentStep === 0) {
        document.getElementById("skipStartIcon").classList.remove("active");
        document.getElementById("skipPrevIcon").classList.remove("active");
        document.getElementById("playPauseContainer").classList.add("active");
        document.getElementById("skipNextIcon").classList.add("active");
        document.getElementById("skipEndIcon").classList.add("active");
    } else if (currentStep === stepList.length) {
        document.getElementById("skipStartIcon").classList.add("active");
        document.getElementById("skipPrevIcon").classList.add("active");
        document.getElementById("playPauseContainer").classList.remove("active");
        document.getElementById("skipNextIcon").classList.remove("active");
        document.getElementById("skipEndIcon").classList.remove("active");
    } else {
        document.getElementById("skipStartIcon").classList.add("active");
        document.getElementById("skipPrevIcon").classList.add("active");
        document.getElementById("playPauseContainer").classList.add("active");
        document.getElementById("skipNextIcon").classList.add("active");
        document.getElementById("skipEndIcon").classList.add("active");
    }
}

function generateNodes() {

    let firstTime = false;

    if (nodes.length === 0) {
        for (let i = 0; i < displayNodes.length; i++) {
            nodes[i] = new Node(i);
            displayNodes[i] = document.createElementNS("http://www.w3.org/2000/svg", "circle");
            displayNodes[i].setAttribute("r", "3%");
            displayNodes[i].setAttribute("id", "node" + i);
            svg.appendChild(displayNodes[i]);
            nodeTxt[i] = document.createElementNS("http://www.w3.org/2000/svg", "text");
            nodeTxt[i].setAttribute("id", "nodeTxt" + i);
            nodeTxt[i].innerHTML = i;
            svg.appendChild(nodeTxt[i]);
        }
        firstTime = true;
    } else {
        for (let node of nodes) {
            node.visited = false
        }
    }

    let newPositions = new Array(nodes.length);

    //Position nodes on center positions
    let tmpArray = [].concat(...displayNodes);
    for (let x = 0; x < 2; x++) {
        for (let y = 0; y < 2; y++) {
            let randomIndex = randomInt(0, tmpArray.length - 1);
            let node = tmpArray.splice(randomIndex, 1)[0];
            newPositions[parseInt(node.id.replace("node", ""))] = [(40 + 20 * x) + "%", (40 + 20 * y) + "%"];
        }
    }

    /*
    Grid:
          x 0   1   2   3
    y
    0       0   1   2   3
    1       4           5
    2       6           7
    3       8   9   10  11Pos
     */

    //Position nodes on random outer positions
    let availablePos = [...Array(12).keys()];
    while (tmpArray.length !== 0) {
        let node = tmpArray.pop();
        let coords = getCoordsFromPos(availablePos.splice(randomInt(0, availablePos.length - 1), 1)[0]);
        newPositions[parseInt(node.id.replace("node", ""))] = [((coords[0] + 1) * 20) + "%", ((coords[1] + 1) * 20) + "%"]
    }

    //Apply random offset
    for (let i = 0; i < newPositions.length; i++) {
        let x = parseFloat(newPositions[i][0].replace("%", ""));
        let y = parseFloat(newPositions[i][1].replace("%", ""));
        x += randomFloat(-1, 2) * xOffset;
        y += randomFloat(-1, 2) * yOffset;
        newPositions[i] = [x + "%", y + "%"];
    }

    if (firstTime) {
        let timeline = anime.timeline({
            easing: "easeOutBack",
            duration: 300,
            complete: function () {
                generateEdges();
            }
        });

        for (let i = 0; i < newPositions.length; i++) {
            displayNodes[i].setAttribute("cx", newPositions[i][0]);
            displayNodes[i].setAttribute("cy", newPositions[i][1]);
            nodeTxt[i].setAttribute("x", newPositions[i][0]);
            nodeTxt[i].setAttribute("y", newPositions[i][1]);
            timeline.add({
                targets: displayNodes[i],
                r: [0, displayNodes[i].getAttribute("r")],
                opacity: [0.5, 1]
            }, 50 * i).add({
                targets: nodeTxt[i],
                opacity: [0, 1],
                duration: 150
            }, "-=100")
        }
    } else {
        let timeline = anime.timeline({
            easing: "easeInOutCubic",
            duration: 500,
            begin: function () {
                resetEdges();
            },
            complete: function () {
                generateEdges();
            }
        });

        for (let i = 0; i < newPositions.length; i++) {
            let lastposition = [displayNodes[i].getAttribute("cx"), displayNodes[i].getAttribute("cy")];
            timeline.add({
                targets: displayNodes[i],
                cx: [lastposition[0], newPositions[i][0]],
                cy: [lastposition[1], newPositions[i][1]],
                fill: "#3e3e3e",
                strokeWidth: 0
            }, 30 * i).add({
                targets: nodeTxt[i],
                x: [lastposition[0], newPositions[i][0]],
                y: [lastposition[1], newPositions[i][1]],
                fill: "#cccccc"
            }, "-=500");
        }
    }
}

function generateEdges() {
    //Generate edges
    let middleNodes = getMiddleNodes();
    for (let node of middleNodes) {
        let algoNode = nodes[parseInt(node.id.replace("node", ""))];

        for (let vertex of nodes) {
            if (algoNode.id === vertex.id || algoNode.adjacentNodes.includes(vertex)) {

            } else {
                let vertexDisplay = document.getElementById("node" + vertex.id);

                let gridCoordsNode = getGridCoords(node);
                let gridCoordsVertex = getGridCoords(vertexDisplay);
                if (gridCoordsNode[0] === gridCoordsVertex[0]) { //Remove overlap of node and edge
                    if (Math.abs(gridCoordsNode[1] - gridCoordsVertex[1]) > 1) {
                        continue;
                    }
                } else if (gridCoordsNode[1] === gridCoordsNode[1]) {
                    if (Math.abs(gridCoordsNode[0] - gridCoordsVertex[0]) > 1) {
                        continue;
                    }
                }

                let distance = 0;
                distance += Math.abs(getGridCoords(vertexDisplay)[0] - getGridCoords(node)[0]);
                distance += Math.abs(getGridCoords(vertexDisplay)[1] - getGridCoords(node)[1]);

                if (randomFloat(0, 1) < (algoNode.adjacentNodes.length + vertex.adjacentNodes.length) * 0.1) {
                    continue;
                }
                if (randomFloat(0, 1) < distance * 0.35) {
                    continue;
                }

                createEdge(nodes[parseInt(node.id.replace("node", ""))], vertex);
            }
        }
    }

    //Connect nodes without edge
    for (let node of nodes) {
        if (node.adjacentNodes.length === 0) {
            let closestNode = nodes[parseInt(getClosestMiddleNode(document.getElementById("node" + node.id)).id.replace("node", ""))];

            createEdge(node, closestNode);
        }
    }
    sleep(400).then(() => blockReset = false)
}

function createEdge(node1, node2) {
    let displayEdge = document.createElementNS("http://www.w3.org/2000/svg", "line");
    displayEdges.push(displayEdge);

    displayEdge.setAttribute("id", getEdgeId(node1, node2));
    displayEdge.classList.add("edge");

    let displayNode1 = document.getElementById("node" + node1.id);
    let displayNode2 = document.getElementById("node" + node2.id);

    displayEdge.setAttribute("x1", displayNode1.getAttribute("cx"));
    displayEdge.setAttribute("y1", displayNode1.getAttribute("cy"));

    displayEdge.setAttribute("x2", displayNode2.getAttribute("cx"));
    displayEdge.setAttribute("y2", displayNode2.getAttribute("cy"));

    if (directed) {
        switch (randomInt(0, 2)) {
            case 0:
                node1.adjacentNodes.push(node2);
                node2.adjacentNodes.push(node1);
                displayEdge.setAttribute("marker-start", "url(#startMarker)");
                displayEdge.setAttribute("marker-end", "url(#endMarker)");
                break;
            case 1:
                node1.adjacentNodes.push(node2);
                displayEdge.setAttribute("marker-end", "url(#endMarker)");
                break;
            case 2:
                node2.adjacentNodes.push(node1);
                displayEdge.setAttribute("marker-start", "url(#startMarker)");
                break;
        }
    } else {
        node1.adjacentNodes.push(node2);
        node2.adjacentNodes.push(node1);
    }

    svg.insertBefore(displayEdge, svg.firstChild);

    anime({
        targets: displayEdge,
        x2: [displayEdge.getAttribute("x1"), displayEdge.getAttribute("x2")],
        y2: [displayEdge.getAttribute("y1"), displayEdge.getAttribute("y2")],
        easing: "easeOutCubic",
        duration: 200 * randomFloat(1, 2)
    });
}

function resetEdges() {
    while (displayEdges.length !== 0) {
        let edge = displayEdges.pop();
        anime({
            targets: edge,
            x2: edge.getAttribute("x1"),
            y2: edge.getAttribute("y1"),
            opacity: {value: 0, duration: 50, delay: 50},
            duration: 100,
            easing: "easeInOutQuad",
            complete: function (anim) {
                anim.animatables[0].target.remove();
            }
        });
    }

    for (let node of nodes) {
        node.adjacentNodes = [];
    }
}

function getEdgeId(nodeA, nodeB) {
    if (nodeA.id < nodeB.id) {
        return "edge" + nodeA.id + "-" + nodeB.id;
    } else {
        return "edge" + nodeB.id + "-" + nodeA.id;
    }
}

function getMiddleNodes() {
    let middleNodes = [];
    for (let node of displayNodes) {
        let coords = getGridCoords(node);
        if ((coords[0] === 1 || coords[0] === 2) && (coords[1] === 1 || coords[1] === 2)) {
            middleNodes.push(node);
        }
    }
    return middleNodes;
}

function getClosestMiddleNode(node) {
    let coords = getGridCoords(node);
    let middleNodes = getMiddleNodes();

    if ((coords[0] === 1 || coords[0] === 2) && (coords[1] === 1 || coords[1] === 2)) { //Check if node is a middleNode itself
        if (coords[1] === 1) {
            coords[1] = 2;
        } else {
            coords[1] = 1;
        }
    } else {
        if (coords[0] <= 1 && coords[1] <= 1) { //top left
            coords = [1, 1];
        } else if (coords[0] >= 2 && coords[1] <= 1) { //top right
            coords = [2, 1];
        } else if (coords[0] <= 1 && coords[1] >= 2) { //bottom left
            coords = [1, 2];
        } else { //bottom right
            coords = [2, 2];
        }
    }


    for (let middleNode of middleNodes) {
        let gridcoords = getGridCoords(middleNode);
        if (gridcoords[0] === coords[0] && gridcoords[1] === coords[1]) {
            return middleNode;
        }
    }
}

function getCoordsFromPos(pos) {
    switch (pos) {
        case 0:
            return [0, 0];
        case 1:
            return [1, 0];
        case 2:
            return [2, 0];
        case 3:
            return [3, 0];
        case 4:
            return [0, 1];
        case 5:
            return [3, 1];
        case 6:
            return [0, 2];
        case 7:
            return [3, 2];
        case 8:
            return [0, 3];
        case 9:
            return [1, 3];
        case 10:
            return [2, 3];
        case 11:
            return [3, 3];
    }
}

function getGridCoords(displayNode) {
    let result = new Array(2);
    let bounds = [svg.getBoundingClientRect().width, svg.getBoundingClientRect().height]
    let coords = [
        displayNode.getBoundingClientRect().x + displayNode.getBoundingClientRect().width / 2,
        displayNode.getBoundingClientRect().y + displayNode.getBoundingClientRect().height / 2
    ];
    coords[0] -= svg.getBoundingClientRect().x;
    coords[1] -= svg.getBoundingClientRect().y;

    for (let axis = 0; axis < coords.length; axis++) {
        for (let i = 0; i < 4; i++) {
            if (coords[axis] > bounds[axis] * (0.1 + 0.2 * i) && coords[axis] < bounds[axis] * (0.3 + 0.2 * i)) {
                result[axis] = i;
                break;
            }
        }
    }
    return result;
}