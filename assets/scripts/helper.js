function sleep(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}

function randomInt(from, range) {
    return Math.floor(Math.random() * (range + 1)) + from;
}

function randomFloat(from, range) {
    return Math.random() * range + from;
}

function reverseRange(value, min, max) {
    let difference = max - value;
    return min + difference;
}